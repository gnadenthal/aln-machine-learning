// ALN Library
// Copyright (C) 1995 -2010 William W. Armstrong
// See license in string szWarning below

// checklicense.cpp

///////////////////////////////////////////////////////////////////////////////
//  File version info:
// 
//  $Archive: /ALN Development/libaln/src/checklicense.cpp $
//  $Workfile: checklicense.cpp $
//  $Revision: 8 $
//  $Date: 7/18/07 1:40p $
//  $Author: Arms $
//
///////////////////////////////////////////////////////////////////////////////

#ifdef ALNDLL
#define ALNIMP __declspec(dllexport)
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _WIN32
#define WIN32_EXTRA_LEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

static char szWarning[] = "Copyright (C) 1995 - 2010 William W. Armstrong."
"This library is free software; you can redistribute it and/or"
"modify it under the terms of the GNU Lesser General Public"
"License as published by the Free Software Foundation; either"
"version 2.1 of the License, or (at your option) any later version."
"This library is distributed in the hope that it will be useful,"
"but WITHOUT ANY WARRANTY; without even the implied warranty of"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU"
"Lesser General Public License for more details."
"You should have received a copy of the GNU Lesser General Public"
"License along with this library; if not, write to the Free Software"
"Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA"
"For further information contact" 
"William W. Armstrong"
"3624 - 108 Street NW"
"Edmonton, Alberta, Canada  T6J 1B4";

#ifdef _WIN32
// show license in message box
static void ShowWarning()
{
  MessageBox(NULL, szWarning, "Copyright (C) 1995 - 2010 William W. Armstrong", MB_OK);
}
#endif // _WIN32

BOOL CheckLicense()
{
#ifdef _WIN32
  ShowWarning();
#endif
  return TRUE;  // assume OK
}
